package com.epam.rd;

import com.epam.rd.domain.Author;
import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import java.util.List;

public class App {

    private static final String PERSISTENCE_UNIT_NAME = "author_PU";

    public static void main(String[] args) {
        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory(PERSISTENCE_UNIT_NAME);
        Long id = addAuthor(entityManagerFactory);

        EntityManager em = entityManagerFactory.createEntityManager();
        System.out.println(em.find(Author.class, id));

        printCacheInfo();

        em = entityManagerFactory.createEntityManager();
        System.out.println(em.find(Author.class, id));
    }

    private static void printCacheInfo() {
        List<CacheManager> cacheManagers = CacheManager.ALL_CACHE_MANAGERS;
        if (!cacheManagers.isEmpty()) {
            CacheManager cacheManager = cacheManagers.get(0);
            Cache authorsCache = cacheManager.getCache(Author.class.getName());
            System.out.println("Authors second level cache has size = " + authorsCache.getSize());
        } else {
            System.out.println("Hibernate second level cache is disabled.");
        }
    }

    private static Long addAuthor(EntityManagerFactory entityManagerFactory) {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        EntityTransaction tx = entityManager.getTransaction();
        try {
            tx.begin();

            Author author = new Author();
            author.setFirstName("Stephen");
            author.setLastName("King");

            entityManager.persist(author);

            tx.commit();
            return author.getId();
        } catch (Exception e) {
            tx.rollback();
            throw new RuntimeException(e);
        }
    }

}
